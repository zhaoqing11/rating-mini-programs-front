
export default {
  login: '/pages/login/main',
  main: '/pages/main/main',
  rule: "/pages/rule/main",
  authorize: "/pages/authorize/main",
  userList: '/pages/userList/main',
  ratingDetail: '/pages/ratingDetail/main',
  groups: '/pages/groups/main',
  noticeList: '/pages/noticeList/main',
  noticeDetail: '/pages/noticeDetail/main',
  error: "/pages/error/main",
  scoringRecord: '/pages/scoringRecord/main',
}
