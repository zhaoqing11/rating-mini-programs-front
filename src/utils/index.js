function formatNumber (n) {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}

function isEmptyString (val) {
  if (val === '' || val === null) {
    return true
  }
}

export function formatTime (date) {
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  const t1 = Number(hour * 3600) + Number(minute * 60) + Number(second)

  return `${t1}`
}

export function getDay () {
  const hour = 6
  const minute = 0
  const second = 0
  const t1 = Number(hour * 3600) + Number(minute * 60) + Number(second)
  return `${t1}`
}

export function getNight () {
  const hour = 17
  const minute = 59
  const second = 59
  const t2 = Number(hour * 3600) + Number(minute * 60) + Number(second)
  return `${t2}`
}

export function formatDuring (mss) {
  var hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
  var minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60))
  var seconds = (mss % (1000 * 60)) / 1000

  hours = hours < 10 ? ('0' + hours) : hours
  minutes = minutes < 10 ? ('0' + minutes) : minutes
  seconds = seconds < 10 ? ('0' + seconds) : seconds
  return hours + ':' + minutes + ':' + seconds
}

export default {
  formatNumber,
  isEmptyString,
  formatTime,
  getDay,
  getNight,
  formatDuring
}
