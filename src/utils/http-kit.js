import Fly from 'flyio/dist/npm/wx'
import router from '@/router/index'
// import store from '@/store/store'

const fly = new Fly()
const host = process.env.HOME_URL
fly.config.baseURL = host
fly.config.headers['content-type'] = 'application/json'
fly.config.timeout = 60 * 1000

/* URL地址token白名单 */
const URL_WHITE_TOKEN_LIST = [
  '/user/UserController/createUser',
  '/district/DistrictController/getDistrict',
  '/role/RoleController/getRoleList',
  '/wx/code/getCode'
]

/**
 * 添加请求拦截器
 */
fly.interceptors.request.use((request) => {
  // todo 需要禁止用户触发请求显示加载中

  /* 当前请求地址不是白名单，则增加请求头token */
  if (URL_WHITE_TOKEN_LIST.indexOf(request.url) === -1) {
    request.headers['token'] = mpvue.getStorageSync('userToken')
  }

  request.body && Object.keys(request.body).forEach((val) => {
    if (request.body[val] === '') {
      delete request.body[val]
    }
  })

  request.body = {
    ...request.body
  }
  return request
}, error => {
  mpvue.hideLoading()

  return Promise.reject(error) // 在调用的那边可以拿到(catch)你想返回的错误信息
})

/**
 * 添加响应拦截器
 */
fly.interceptors.response.use(res => {
    mpvue.hideLoading()

    /* 根据返回的code值来做不同的处理（和后端约定） */
    // let data = res.data
    switch (res.data.status) {
      case 200:
        return Promise.resolve(res)
      default:
        return Promise.reject(res)
    }
  }, error => {
    mpvue.hideLoading()
    let isShowModal = error.request.isShowModal === undefined || error.request.isShowModal

    if (error.status === 1 && error.message.indexOf('timeout') >= 0) {
      // /* 请求超时咨询是否重试 */
      // mpvue.showModal({
      //   title: '提示',
      //   content: `网络请求超时，是否重试`,
      //   success: (r) => {
      //     if (r.confirm) {
      //     }
      //   }
      // })
    } else {
      if (error && error.response) {
        switch (error.response.status) {
          case 401:
            mpvue.redirectTo({url: router.login});
         /*   mpvue.showModal({
              title: '提示',
              content: `登录超时，请重新登录`,
              showCancel: false,
              success: (d) => {
                mpvue.redirectTo({url: router.login})
              }
            })*/
            return
          case 500:
            isShowModal && mpvue.showModal({title: '提示', content: `服务端异常，如有疑问请联系客服`, showCancel: false})
            return Promise.reject(error.message)
          default:
            isShowModal && mpvue.showModal({title: '提示', content: `网络异常，如有疑问请联系客服`, showCancel: false})
            return
        }
      }
    }

    // todo add by 梁展鹏 20190830 拦截器异常对象该怎么处理？
    return Promise.reject(error.message)
  }
)

export default fly
