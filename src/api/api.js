import HttpKit from '@/utils/http-kit'

export default {

  /** ===================（score）===================== */

  /**
   * desc: 根据userid查询用户评分列表
   */
  selectScoreByIdUser(data) {
    return HttpKit.get(`/score/ScoreController/selectScoreByIdUser`, data).then(
      res => res.data
    )
  },

  /**
   * desc: 获取评分人评分记录
   */
  selectScoreById(data) {
    return HttpKit.get(`/score/ScoreController/selectScoreById`, data).then(
      res => res.data
    )
  },

  /** ===================（answer）===================== */

  /**
   * desc: 根据评分记录编号查询评分答卷详情
   */
  selectAnswerByIdScore(data) {
    return HttpKit.get(`/answer/AnswerController/selectAnswerByIdScore`, data).then(
      res => res.data
    )
  },

  /**
   * desc: 新增答卷记录
   */
  insertAnswer(data) {
    return HttpKit.post(`/answer/AnswerController/insertAnswer`, data).then(
      res => res.data
    )
  },
  
  /** ===================（question）===================== */

  /**
   * desc: 获取问题字典列表
   */
  getQuestionList(data) {
    return HttpKit.get(`/question/QuestionController/getQuestionList`, data).then(
      res => res.data
    )
  },

  /** ===================（department）===================== */

  /**
   * desc: 获取部门列表
   */
  getDepartments() {
    return HttpKit.get(`/department/DepartmentController/getDepartmentList`).then(
      res => res.data
    )
  },

  /** ===================（notice）===================== */

  /**
    * desc: 修改通知阅读状态
    * @param {*} form 
    */
  updateCheckedState(data) {
    return HttpKit.post(`/notice/NoticeController/updateNotice`, data).then(
      res => res.data
    )
  },

  /**
    * desc: 获取用户通知列表
    * @param {*} form 
    */
  getNoticeList(data) {
    return HttpKit.get(`/notice/NoticeController/getNoticesByIdUser`, data).then(
      res => res.data
    )
  },
  
  /**
    * desc: 根据id获取公告详情
    * @param {*} form 
    */
  getNoticeDetail(data) {
    return HttpKit.get(`/notice/NoticeController/getNoticeDetail`, data).then(
      res => res.data
    )
  },

  /** ===================（role）===================== */

  /**
   * 功能描述：获取角色列表
   */
  getRoleList() {
    return HttpKit.get(`/role/RoleController/getRoleList`).then(
      res => res.data
    )
  },


  /** ===================（user）===================== */

  /**
   * 根据idUser获取个人信息
   * @param {*} data 
   */
  selectUserById(data) {
    return HttpKit.get(`/user/UserController/selectUserById`, data).then(
      res => res.data
    )
  },

  /**
   * 获取部门员工列表
   * @param {*} data 
   */
  getDepartmentUser(data) {
    return HttpKit.get(`/user/UserController/getDepartmentUser`, data).then(
      res => res.data
    )
  },

  /**
   * 批量用户密码加密
   */
  encryptedPassword() {
    return HttpKit.post(`/user/UserController/encryptedPassword`).then(
      res => res.data
    )
  },
  
  /**
   * 获取用户绑定手机号
   * @param {*} data 
   */
  getPhoneNumber(data) {
    return HttpKit.post(`/get-wx-api/WxOpenIdController/getPhoneNumber`, data).then(
      res => res.data
    )
  },

  /**
   * 获取用户唯一标识openid
   * @param {*} data 
   */
  getOpenId(data) {
    return HttpKit.get(`/get-wx-api/WxOpenIdController/getOpenId`, data).then(
      res => res.data
    )
  },

   /**
   * 功能描述：获取微信openId
   */
  getOpenId(data) {
    return HttpKit.get(`/get-wx-api/WxOpenIdController/getOpenId`, data).then(
      res => res.data
    )
  },

  /**
   * 功能描述：获取上级管理员
   */
  getManagerMsg(data) {
    return HttpKit.get(`/user/UserController/getManagerMsg`, data).then(
      res => res.data
    )
  },
  
  /**
   * 功能描述：获取短信验证码
   * @param {*} data 
   */
  getSmsCode(data) {
    return HttpKit.get(`/user/UserController/getSmsCode`, data).then(
      res => res.data
    )
  },
  
  /**
   * 功能描述：修改用户密码
   * @param {*} data 
   */
  updatePassword(data) {
    return HttpKit.get(`/user/UserController/updatePassword`, data).then(
      res => res.data
    )
  },
  
  /**
   * 功能描述：验证账号旧密码
   * @param {*} data 
   */
  checkPassword(data) {
    return HttpKit.get(`/user/UserController/checkPassword`, data).then(
      res => res.data
    )
  },
  
  
  /**
   * 功能描述：根据身份ID获取群成员用户列表
   * @param {*} data 
   */
  getGroupMemberByRole(data) {
    return HttpKit.get(`/user/UserController/getGroupMemberByRole`, data).then(
      res => res.data
    )
  },

  /**
   * 功能描述：获取天河区下的街道监管信息
   * @param {*} data 
   */
  getCheckedProjects(data) {
    return HttpKit.get(`/user/UserController/getCheckedProject`, data).then(
      res => res.data
    )
  },
  
  /**
   * 功能描述：获取除业主和管理员外的街道监管用户待审核列表
   * @param {*} data 
   */
  getDistrictUser() {
    return HttpKit.get(`/user/UserController/getDistrictUser`).then(
      res => res.data
    )
  },

    /**
   * 功能描述：管理员登录
   * @param {*} data 
   */
  login(data) {
    return HttpKit.post(`/user/UserController/login`, data).then(
      res => res.data
    )
  },

  /**
   * 功能描述：获取群成员列表
   * @param {*} data 
   */
  getGroupMember(data) {
    return HttpKit.get(`/user/UserController/getGroupMember`, data).then(
      res => res.data
    )
  },

   /**
   * 功能描述：查询当前用户的 下级用户项目信息列表
   * @param {*} data 
   */
  getUserByDistrictId(data) {
    return HttpKit.get(`/user/UserController/getUserByDistrictId`, data).then(
      res => res.data
    )
  },

  /**
   * 功能描述：修改注册信息
   * @param {*} data 
   */
  updateUser(form) {
    return HttpKit.post(`/user/UserController/updateUser`, form).then(
      res => res.data
    )
  },

  /**
   * 功能描述：根据userId查询用户基本信息
   * @param {*} data 
   */
  selectById(data) {
    return HttpKit.get(`/user/UserController/selectById`, data).then(
      res => res.data
    )
  },
  
    /**
   * 功能描述：查询当前区域下待审核注册列表
   * @returns
   */
  getCheckPending (data) {
    return HttpKit.get(`/user/UserController/getCheckPending`, data).then(
      res => res.data
    )
  },

  /**
   * 功能描述：注册用户
   * @returns {form 用户填表信息}
   */
  createUser (form) {
    return HttpKit.post(`/user/UserController/createUser`, form).then(
      res => res.data
    )
  },

  /**
   * 功能描述： 查询用户列表
   */
  selectAll () {
    return HttpKit.get(`/user/UserController/selectAll`).then(
      res => res.data
    )
  }
}