// https://vuex.vuejs.org/zh-cn/intro.html
// make sure to call Vue.use(Vuex) if using a module system
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    groupMember: 0,
    groupForm: {},
    userInfo: {
      userId: '',
      username: '',
      phone: '',
      password: '',
      roleId: -1
    },
    checked: 1,
    systemNoticeState: 0
  },
  mutations: {
    setChecked (state, params) {
      state.checked = params
    },
    setSystemNoticeState (state, params) {
      state.systemNoticeState = params
    },
    setGroupForm (state, params) {
      state.groupForm = params
    },
    setUserInfo (state, params) {
      state.userInfo = params
    },
    setGroupMember (state, params) {
      state.groupMember = params
    }
  },
  actions: {
    setChecked (context, params) {
      context.commit('setChecked', params)
    },
    setSystemNoticeState (context, params) {
      context.commit('setSystemNoticeState', params)
    },
    setGroupForm (context, params) {
      context.commit('setGroupForm', params)
    },
    setUserInfo (context, params) {
      context.commit('setUserInfo', params)
    },
    setGroupMember (context, params) {
      context.commit('setGroupMember', params)
    }
  }
})
export default store