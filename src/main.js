import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
/*import fastclick from 'fastclick'
Vue.use(fastclick)*/

/* 引入weui */
import '../static/weui/weui.wxss'
/* 引入自定义css */
import './assets/css/flex.css'
/* 引入iconfont.css */
import './assets/iconfont/iconfont.css'

Vue.prototype.$store = store
Vue.config.productionTip = false
App.mpType = 'app'

// const app = new Vue(App)
const app = new Vue({
    App,
    router,
    store
})
app.$mount()
